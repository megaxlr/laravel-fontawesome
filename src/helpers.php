<?php
if(!function_exists('fa')) {
	function fa(string $name, string $type='') {
		return FontAwesome::make($name, $type);
	}
}
