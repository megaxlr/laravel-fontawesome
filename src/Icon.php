<?php
namespace MegaXLR\LaravelFontAwesome;

use Illuminate\Contracts\Support\Htmlable;

class Icon implements Htmlable {

	private $name = '',
			$prefix = '',
			$tagname = '',
			$type = '',
			$version = ''; //FontAwesome version

	public function __construct(string $name, string $prefix, string $tagname, string $version, $type='fa') {
		$this->name = $name;
		$this->prefix = $prefix;
		$this->tagname = $tagname;
		$this->version = $version;
		$this->type = $type;
		$this->template = '<%3$s class="%4$s %1$s%\-%2$s"></%3$s>';
	} 

	private $template = "";

	public function __toString(): string {
		if($this->version[0] != "5") $venpref = $this->prefix;
		else $venpref = $this->type;
		return sprintf($this->template, htmlentities($this->prefix), htmlentities($this->name), htmlentities($this->tagname), $venpref);
	}
	
	public function toHtml(): string {
		return $this->__toString();
	}

}