<?php 

namespace MegaXLR\LaravelFontAwesome;

use MegaXLR\LaravelFontAwesome\Icon;

class FontAwesome {
	private static $prefix = "fa";
	private static $tagname = "i";
	private static $type = "far";
	private static $version = "5.0.6"; // FontAwesome version for Compatibility
	private static $types = ['fa', 'far', 'fas', 'fal', 'fab'];

	public static function setPrefix(string $name='fa') {
		self::$prefix = $name;
	}
	public static function setType(string $name='fa') {
		if(!in_array($name, self::$types)) throw new \Exception("FontAwesome prefix $name does not exist.", 1);
		self::$type = $name;
	}	

	public static function setVersion(string $name='5.0.6') {
		self::$version = $name;
	}

	public static function setTagName(string $name="i") {
		self::$tagname = $name;
	}

	public static function make(string $name, string $type='') {
		if(empty($type) || !in_array($type, self::$types)) $type = self::$type;
		return new Icon($name, self::$prefix, self::$tagname, self::$version, $type);
	}
}